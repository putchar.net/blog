flake_update:
	nix flake update

zola_serve:
	zola serve

zola_build:
	zola build

nix_build:
	nix build

garbage_collect:
	nix-collect-garbage -d
	sudo nix-collect-garbage -d
