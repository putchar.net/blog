+++
title = "putchar.net"

## https://www.markdownguide.org/basic-syntax/
# The homepage contents
[extra]
lead = 'personal blog, about ops stuff, nix and nixos.'

[[extra.list]]
title = "Blog"
content = "Simple topics."
url = "/blog/"

[[extra.list]]
title = "Docs"
content = "More in depth content."
url = "/docs/"

+++
