+++
title = "Nix - Introduction"
description = "A brief explanation about nix and nixos"
date = 2022-01-14
updated = 2022-01-16
draft = false
template = "blog/page.html"
slug = "nix-intro"

[taxonomies]
authors = ["putch4r"]

+++

I have been using **nix** for quite some time now.

And i have been amazed at all i can do with a single language.

Be it configuring my own machines in a programatic way.
Hacking some stuff, breaking some stuff and being able to rollback if needed.

In this first post i will try to explain a bit of nix and its principles

##### What is nix, and how nix works

cf: [nixos.org/guides/how-nix-works](https://nixos.org/guides/how-nix-works.html)

Nix is both a language, and a package manager.

##### Nix the language is a lazy functional language

As any primarily functional language, it takes one or multiple inputs and return one output.

And as any lazy language, your variables, your code will only be evaluated when it is needed.

Every nix functions are immutable by nature and given the same input will always return the same output.


##### Nix the package manager is a *purely functional* package manager

And as such to build a **package** it will take one or multiple inputs, and generate an output.

For instance if i build [lsd-0.20.1](href="https://github.com/Peltoche/lsd") package, the build output will look like this

```ps1
/nix/store/8w8l61fc2jydk230k1mk0mbvc7w3z03k-lsd-0.20.1
```

The hash in front of the name is a unique identifier.

**nix** generated this unique hash from the input of the build of the package `lsd` (it is actually a bit more complicated but we will see that later)

If i want to build another version of `lsd` (for instance the version `0.18.0`) the output will be different

```ps1
/nix/store/q57f6x2ygpcnlnbfd57y3qbglymyz1af-lsd-0.18.0
```

##### With nix you can have multiple versions of a package

Here is the list of all of my python runtime installed in my machine
```ps1
[ λ /nix/store ]
20:00 $ ls -la | grep "python3-3" | grep -v "(env|drv)"
dr-xr-xr-x root    root    4.0 KB Thu Jan  1 01:00:01 1970 4w1rxv6wfkv728jzmm6pqq3jsqd4yvi7-python3-3.7.3
dr-xr-xr-x root    root    4.0 KB Thu Jan  1 01:00:01 1970 5bh6rpya1ar6l49vrhx1rg58dsa42906-python3-3.9.6
dr-xr-xr-x root    root    4.0 KB Thu Jan  1 01:00:01 1970 6cfajs6lsy9b4wxp3jvyyl1g5x2pjmpr-python3-3.8.9
dr-xr-xr-x root    root    4.0 KB Thu Jan  1 01:00:01 1970 81lwy2hfqj4c1943b1x8a0qsivjhdhw9-python3-3.9.6
dr-xr-xr-x root    root    4.0 KB Thu Jan  1 01:00:01 1970 dgmp76d54xxb3bka1fph17yz7n8rfkai-python3-3.8.12
dr-xr-xr-x root    root    4.0 KB Thu Jan  1 01:00:01 1970 dn4fwp0yx6nsa85cr20cwvdmg64xwmcy-python3-3.9.9
dr-xr-xr-x root    root    4.0 KB Thu Jan  1 01:00:01 1970 dqxic3j7csd4ywn94n4smmnz55p039g3-python3-3.9.6
dr-xr-xr-x root    root    4.0 KB Thu Jan  1 01:00:01 1970 fkzla307l4mlcvfyshsrccwl7szihx2z-python3-3.9.6
dr-xr-xr-x root    root    4.0 KB Thu Jan  1 01:00:01 1970 k0z9n599k02hab8qjjp3ljw065iwjcvg-python3-3.9.6
dr-xr-xr-x root    root    4.0 KB Thu Jan  1 01:00:01 1970 rppr9s436950i1dlzknbmz40m2xqqnxc-python3-3.9.9
dr-xr-xr-x root    root    4.0 KB Thu Jan  1 01:00:01 1970 wlhmpa2qk786ddp05ahv3jjlds6d44a5-python3-3.8.12
dr-xr-xr-x root    root    4.0 KB Thu Jan  1 01:00:01 1970 z65l1jqvxa58zzwwa3bvglb6asj4y8cv-python3-3.8.5
dr-xr-xr-x root    root    4.0 KB Thu Jan  1 01:00:01 1970 zszhmcfw67yy8psldnz0gpj94z417si0-python3-3.9.9
```
Some of those runtime are standalone, some are part of another package (and are considered a dependency)

For instance `fail2ban` package will bring its own python runtime when it is installed

They are installed. They do not overlaps. There is no shared object nightmare or dll hell.

The package as well as the shared objects and / or its dependencies have all a hash in their name.

for instance with `lsd-0.20.1`, it requires all of these dependencies
```ps1
17:14 $ ldd $(which lsd)
	linux-vdso.so.1 (0x00007fff33aba000)
	libdl.so.2 => /nix/store/s9qbqh7gzacs7h68b2jfmn9l6q4jwfjz-glibc-2.33-59/lib/libdl.so.2 (0x00007fcd92d81000)
	libgcc_s.so.1 => /nix/store/s9qbqh7gzacs7h68b2jfmn9l6q4jwfjz-glibc-2.33-59/lib/libgcc_s.so.1 (0x00007fcd92d67000)
	libpthread.so.0 => /nix/store/s9qbqh7gzacs7h68b2jfmn9l6q4jwfjz-glibc-2.33-59/lib/libpthread.so.0 (0x00007fcd92d47000)
	libm.so.6 => /nix/store/s9qbqh7gzacs7h68b2jfmn9l6q4jwfjz-glibc-2.33-59/lib/libm.so.6 (0x00007fcd92c06000)
	libc.so.6 => /nix/store/s9qbqh7gzacs7h68b2jfmn9l6q4jwfjz-glibc-2.33-59/lib/libc.so.6 (0x00007fcd92a41000)
	/nix/store/s9qbqh7gzacs7h68b2jfmn9l6q4jwfjz-glibc-2.33-59/lib/ld-linux-x86-64.so.2 => /nix/store/s9qbqh7gzacs7h68b2jfmn9l6q4jwfjz-glibc-2.33-59/lib64/ld-linux-x86-64.so.2 (0x00007fcd9301b000)
```

and for `lsd-0.18.0`
```ps1
17:14 $ ldd result/bin/lsd
	linux-vdso.so.1 (0x00007ffc3b592000)
	libdl.so.2 => /nix/store/9df65igwjmf2wbw0gbrrgair6piqjgmi-glibc-2.31/lib/libdl.so.2 (0x00007f7260da9000)
	libpthread.so.0 => /nix/store/9df65igwjmf2wbw0gbrrgair6piqjgmi-glibc-2.31/lib/libpthread.so.0 (0x00007f7260d88000)
	libgcc_s.so.1 => /nix/store/9df65igwjmf2wbw0gbrrgair6piqjgmi-glibc-2.31/lib/libgcc_s.so.1 (0x00007f7260d6e000)
	libc.so.6 => /nix/store/9df65igwjmf2wbw0gbrrgair6piqjgmi-glibc-2.31/lib/libc.so.6 (0x00007f7260baf000)
	/nix/store/9df65igwjmf2wbw0gbrrgair6piqjgmi-glibc-2.31/lib/ld-linux-x86-64.so.2 => /nix/store/s9qbqh7gzacs7h68b2jfmn9l6q4jwfjz-glibc-2.33-59/lib64/ld-linux-x86-64.so.2 (0x00007f7261007000)
	libm.so.6 => /nix/store/9df65igwjmf2wbw0gbrrgair6piqjgmi-glibc-2.31/lib/libm.so.6 (0x00007f7260a6e000)
```

##### Nix is Atomic

Dependencies and package installations are tracked directly in nix through an intermediate format called `derivations`.

```ps1
17:27 $ nix show-derivation $(which lsd)
## the output is obfuscated because it is very huge
{
  "/nix/store/fvz1xhjx9083n17d4fvrlzswgxcd9rnm-lsd-0.20.1.drv": {
    "outputs": {
      "out": {
        "path": "/nix/store/8w8l61fc2jydk230k1mk0mbvc7w3z03k-lsd-0.20.1"
      }
    },
    "inputSrcs": [
      "/nix/store/9krlzvny65gdc8s7kpb6lkx8cd02c25b-default-builder.sh",
      "/nix/store/nk6b2ckznjic5wj8ddw0wgdrn4mbz3lg-patch-registry-deps"
    ],
    "inputDrvs": {
      "/nix/store/1il9b1hyrzswyrbvdn9plag1h5i7varb-git-2.34.1.drv": [
        "out"
      ],
      "/nix/store/4kwr857ilz3xdbkrwdyppkfd92ihx6n9-rustc-1.57.0.drv": [
        "out"
      ],
      "/nix/store/s6h9m4j52lf843476jn15z2vylz8nbib-cargo-build-hook.sh.drv": [
        "out"
      ],
      "/nix/store/vbvipbc30m7pd9rqqdas1a80dfwlrmma-install-shell-files.drv": [
        "out"
      ]
    }
  }
}
```

##### With nix, software and package are installed into *unique directories* inside the nix store


```ps1
17:43 $ ll $(which lsd)
lrwxrwxrwx root root 62 B Thu Jan  1 01:00:01 1970  /home/putchar/.nix-profile/bin/lsd ⇒ /nix/store/8w8l61fc2jydk230k1mk0mbvc7w3z03k-lsd-0.20.1/bin/lsd


17:43 $ tree /nix/store/8w8l61fc2jydk230k1mk0mbvc7w3z03k-lsd-0.20.1/
/nix/store/8w8l61fc2jydk230k1mk0mbvc7w3z03k-lsd-0.20.1/
├── bin
│   └── lsd
└── share
    ├── bash-completion
    │   └── completions
    │       └── lsd.bash
    ├── fish
    │   └── vendor_completions.d
    │       └── lsd.fish
    └── zsh
        └── site-functions
            └── _lsd

8 directories, 4 files
```
They are then symlinked into the user `PATH` or the system `PATH` (more on that later)

At the cost of **greater storage requirements**, all upgrades / installs in Nix are guaranteed to be both **atomic**, **immutable** and capable of **efficient roll-back**.



##### Ok so ... What is NixOS ?

Well you take all of those principles and you apply them at the Operating System level.

This is **NixOS**

An **immutable**, **stateless** and **atomic** operating system

for more information you can take a look at the [nix glossary](/docs/glossary/nix/#nix)
