+++
title = "Let's install NixOS"
description = "Simple NixOS installation"
date = 2022-01-16
updated = 2022-12-16
draft = true
template = "blog/page.html"
slug = "nixos-install"

[taxonomies]
authors = ["putch4r"]

[extra]
lead = "In this article, we will do a simple nixos-21.11 install"
+++

## Disclaimer

This tutorial is done on a virtual machine backed by the usual qemu/kvm/libvirt setup.

You can have a similar result/experience in any hypervisor solution (vmware fusion / virtualbox).

Just make sure to verify the name of your volumes inside your vm and adapt this tutorial to your needs.

Also for the sake of simplicity we will using plain legacy boot, and we wont be encrypting our partitions.

You will find more exemple of nixos setup in the [Docs section](/docs) of this website.


## Prepare your iso

Let's go into [nixos.org/download](https://nixos.org/download.html#nixos-iso) page and scroll down a bit

For this tutorial i will use the gnome-64 iso image

{{ resize_image(path="content/blog/01-nixos-install/nixos_org.png",  width=720, height=740, op="fit") }}
