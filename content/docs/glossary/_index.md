+++
title = "Glossary"
description = "A simple definition of some word you can find in this website."
date = 2022-10-14
updated = 2022-10-14
template = "docs/section.html"
sort_by = "weight"
weight = 4
draft = false
+++
