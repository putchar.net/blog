+++
title = "Nix Glossary"
description = "Glossary for nix."
date = 2022-10-14
updated = 2022-10-14
draft = false
weight = 420
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Glossary for nix."
toc = true
top = false

[taxonomies]
authors = ["putch4r"]
+++

## Nix

Nix is both a functional language and a package manager.

## Nixos

* nixos is a linux distribution based on nix.
* it is a source based distribution with a binary cache.
* the default binary cache comes from [cachix](https://www.cachix.org/). Thank you cachix !
* there are two stable releases per year.
* there is also an unstable rolling release (like archlinux and its rolling release).

## Nix channels

A nix channel is what is the closest to a traditional package repository.

Think **apt**, **rpm**, **dnf** but with many more things.

The most common channels are **nixpkgs**, **home-manager**, **nixos-hardware** and perhaps **nur** (nix user repository).

They all have their usecase.


### 1. nixpkgs

* [nixpkgs](https://github.com/NixOS/nixpkgs "nixpkgs github repository") is a channel.
* this is the main repository of **nix** the package manager.
* it contains every officially supported nix packages, as well as **nixos** distribution.
* it contains multiple git branch, usually one for each releases and the main branch.
* you can search for every package of this repository in [search.nixos.org](https://search.nixos.org/ "search nix package") website.
* according to [Repology](https://repology.org/repository/nix_unstable "repology/nixpkgs unstable"), as of January 2022 it contains more than 80 000 packages and is the most up to date repository

Let's take a look at this graph

{{ resize_image(path="content/docs/glossary/repology.png",  width=720, height=740, op="fit") }}

The image is not cropped this is a repology rendering you see


### 2. home-manager

* [home-manager](https://github.com/nix-community/home-manager "home-manager github repository") is a channel.
* it focuses on home user environment configuration.
* it can be installed and used on most nix compatible operating system. (darwin / ubuntu / archlinux and so on)
* more doc is available in the [nix-community.github.io/home-manager/](https://nix-community.github.io/home-manager/) website.

### 3. nixos-hardware

* [nixos-hardware](https://github.com/NixOS/nixos-hardware) is a channel.
* it focuses on hardware basic setup and profile.
* if you a have a dell or a lenovo,
take a look at this repository and maybe you will find an optimal setup for your hardware.

### 4. nur

* [nux](https://github.com/nix-community/NUR) is a channel.
* it behaves more like [aur](https://aur.archlinux.org/) repository.
* it is a community driven channel
* the packages in this channel are not stored in cachix
