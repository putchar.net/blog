{
  description = "A flake for developing and building my personal website";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.adidoks = {
    #url = "github:aaranxu/adidoks?rev=501a1d9766f529f5a89ab0b5bee2b2b73297bdac";
    url = "github:aaranxu/adidoks";
    flake = false;
  };

  outputs = { self, nixpkgs, flake-utils, adidoks }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        themeName = ((builtins.fromTOML
          (builtins.readFile "${adidoks}/theme.toml")).name);
        blogDir = "blog.putchar.net";
      in {
        packages.website = pkgs.stdenv.mkDerivation rec {
          pname = "static-website";
          version = "2021-11-19";
          src = ./.;
          nativeBuildInputs = [ pkgs.zola ];
          configurePhase = ''
            mkdir -p "themes/${themeName}"
            cp -r ${adidoks}/* "themes/${themeName}"
          '';
          buildPhase = "zola build";
          installPhase = "cp -r public $out";
        };
        defaultPackage = self.packages.${system}.website;

        devShell = pkgs.mkShell {
          name = "blog-with-zola";
          packages = [ pkgs.zola pkgs.gnumake ];
          shellHook = ''
            mkdir -p themes
            if [[ -d themes/${themeName} ]]; then
              echo "all good"
            else
              ln -sn "${adidoks}" "themes/${themeName}"
            fi
          '';
        };
      });
}
